using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace NewsReport.Models
{
    public class NewsUser : IdentityUser{
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public class NewsCategory{
        public int NewsCategoryID { get; set; }
        public string ShortName { get; set; }
        public string FullName { get; set; }
    }

    public class News{
        public int NewsID { get; set; }
        public int NewsCategoryID { get; set; }
        public NewsCategory NewsCat { get; set; }


        [DataType(DataType.Date)]
        public string ReportDate { get; set; }
        public string NewsDetail { get; set; }
        public float GpsLat { get; set; }
        public float GpsLng { get; set; }
        public string NewsStatus { get; set; }

        
        public string NewsUserId {get; set;}
        public NewsUser postUser {get; set;}

    }


}