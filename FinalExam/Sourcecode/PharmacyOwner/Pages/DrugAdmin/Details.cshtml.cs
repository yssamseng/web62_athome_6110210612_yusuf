using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PharmacyOwner.Data;
using PharmacyOwner.Models;

namespace PharmacyOwner.Pages.DrugAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly PharmacyOwner.Data.PharmacyContext _context;

        public DetailsModel(PharmacyOwner.Data.PharmacyContext context)
        {
            _context = context;
        }

        public Drug Drug { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Drug = await _context.Drugs
                .Include(d => d.Stores)
                .Include(d => d.postUser).FirstOrDefaultAsync(m => m.DrugID == id);

            if (Drug == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
