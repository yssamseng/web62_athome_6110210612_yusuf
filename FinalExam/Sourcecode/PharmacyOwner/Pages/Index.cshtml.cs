﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PharmacyOwner.Models;
using PharmacyOwner.Data;

namespace PharmacyOwner.Pages
{
    public class IndexModel : PageModel
    {
        private readonly PharmacyOwner.Data.PharmacyContext _context;

        public IndexModel(PharmacyOwner.Data.PharmacyContext context)
        {
            _context = context;
        }

        public IList<Store> Store { get;set; }
        public IList<Drug> Drug { get;set; }

        public async Task <IActionResult> OnGetAsync()
        {
            Store = await _context.Stores
                .Include(s => s.postUser).ToListAsync();
            Drug = await _context.Drugs
                .Include(s => s.Stores)
                .Include(s => s.postUser).ToListAsync();
            return Page();
        }
    }
}
