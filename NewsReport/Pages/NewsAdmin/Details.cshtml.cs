using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using NewsReport.Data;
using NewsReport.Models;

namespace NewsReport.Pages.NewsAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly NewsReport.Data.NewsReportContext _context;

        public DetailsModel(NewsReport.Data.NewsReportContext context)
        {
            _context = context;
        }

        public News News { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            News = await _context.NewsList
                .Include(n => n.NewsCat).FirstOrDefaultAsync(m => m.NewsID == id);

            if (News == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
