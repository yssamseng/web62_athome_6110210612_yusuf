using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using PharmacyOwner.Data;
using PharmacyOwner.Models;
using Microsoft.EntityFrameworkCore;

namespace PharmacyOwner.Pages.DrugAdmin {
    public class CreateModel : PageModel {
        private readonly PharmacyOwner.Data.PharmacyContext _context;

        public CreateModel (PharmacyOwner.Data.PharmacyContext context) {
            _context = context;
        }

        public IList<Store> Store { get;set; }

        public async Task <IActionResult> OnGetAsync () {
            Store = await _context.Stores
                .Include(s => s.postUser).ToListAsync();
            return Page ();
        }

        [BindProperty]
        public Drug Drug { get; set; }

        public async Task<IActionResult> OnPostAsync () {
            if (!ModelState.IsValid) {
                return Page ();
            }

            _context.Drugs.Add (Drug);
            await _context.SaveChangesAsync ();

            return RedirectToPage ("./Index");
        }
    }
}