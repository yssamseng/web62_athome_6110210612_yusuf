using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;
namespace PharmacyOwner.Models {
    public class NewUser : IdentityUser {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public class Store {
        public int StoreID { get; set; }
        public string StoreName { get; set; }
        public string StoreDescribe { get; set; }

        public string NewUserId { get; set; }
        public NewUser postUser { get; set; }
    }
    public class Drug {
        public int StoreID { get; set; }
        public Store Stores { get; set; }

        public int DrugID { get; set; }
        public string DrugName { get; set; }
        public float DrugPrice { get; set; }
        public int DrugLeft { get; set; }

        public string NewUserId { get; set; }
        public NewUser postUser { get; set; }
    }
}