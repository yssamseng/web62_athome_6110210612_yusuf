using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PharmacyOwner.Data;
using PharmacyOwner.Models;

namespace PharmacyOwner.Pages.StoreAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly PharmacyOwner.Data.PharmacyContext _context;

        public DetailsModel(PharmacyOwner.Data.PharmacyContext context)
        {
            _context = context;
        }

        public Store Store { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Store = await _context.Stores
                .Include(s => s.postUser).FirstOrDefaultAsync(m => m.StoreID == id);

            if (Store == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
