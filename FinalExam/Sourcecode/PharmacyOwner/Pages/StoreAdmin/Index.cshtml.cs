using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PharmacyOwner.Data;
using PharmacyOwner.Models;

namespace PharmacyOwner.Pages.StoreAdmin
{
    public class IndexModel : PageModel
    {
        private readonly PharmacyOwner.Data.PharmacyContext _context;

        public IndexModel(PharmacyOwner.Data.PharmacyContext context)
        {
            _context = context;
        }

        public IList<Store> Store { get;set; }

        public async Task OnGetAsync()
        {
            Store = await _context.Stores
                .Include(s => s.postUser).ToListAsync();
        }
    }
}
