using Microsoft.EntityFrameworkCore;
using PharmacyOwner.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace PharmacyOwner.Data {
    public class PharmacyContext : IdentityDbContext<NewUser> {
        public DbSet<Store> Stores { get; set; }
        public DbSet<Drug> Drugs { get; set; }

        protected override void OnConfiguring (DbContextOptionsBuilder optionsBuilder) {
            optionsBuilder.UseSqlite (@"Data source=Pharmacy.db");
        }
    }
}