using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using NewsReport.Data;
using NewsReport.Models;

namespace NewsReport.Pages.NewsAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly NewsReport.Data.NewsReportContext _context;

        public DeleteModel(NewsReport.Data.NewsReportContext context)
        {
            _context = context;
        }

        [BindProperty]
        public News News { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            News = await _context.newsList
                .Include(n => n.NewsCat)
                .Include(n => n.postUser).FirstOrDefaultAsync(m => m.NewsID == id);

            if (News == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            News = await _context.newsList.FindAsync(id);

            if (News != null)
            {
                _context.newsList.Remove(News);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
