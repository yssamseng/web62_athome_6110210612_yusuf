﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NewsReport.Migrations
{
    public partial class NewsDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "NewsCategory",
                columns: table => new
                {
                    NewsCategoryID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ShortName = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NewsCategory", x => x.NewsCategoryID);
                });

            migrationBuilder.CreateTable(
                name: "NewsList",
                columns: table => new
                {
                    NewsID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    NewsCategoryID = table.Column<int>(nullable: false),
                    ReportDate = table.Column<string>(nullable: true),
                    NewsDetail = table.Column<string>(nullable: true),
                    GpsLat = table.Column<float>(nullable: false),
                    GpsLng = table.Column<float>(nullable: false),
                    NewsStatus = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NewsList", x => x.NewsID);
                    table.ForeignKey(
                        name: "FK_NewsList_NewsCategory_NewsCategoryID",
                        column: x => x.NewsCategoryID,
                        principalTable: "NewsCategory",
                        principalColumn: "NewsCategoryID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_NewsList_NewsCategoryID",
                table: "NewsList",
                column: "NewsCategoryID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "NewsList");

            migrationBuilder.DropTable(
                name: "NewsCategory");
        }
    }
}
