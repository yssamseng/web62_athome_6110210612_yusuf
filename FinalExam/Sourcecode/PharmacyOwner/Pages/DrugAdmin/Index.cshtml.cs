using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PharmacyOwner.Data;
using PharmacyOwner.Models;

namespace PharmacyOwner.Pages.DrugAdmin
{
    public class IndexModel : PageModel
    {
        private readonly PharmacyOwner.Data.PharmacyContext _context;

        public IndexModel(PharmacyOwner.Data.PharmacyContext context)
        {
            _context = context;
        }

        public IList<Drug> Drug { get;set; }

        public async Task OnGetAsync()
        {
            Drug = await _context.Drugs
                .Include(d => d.Stores)
                .Include(d => d.postUser).ToListAsync();
        }
    }
}
